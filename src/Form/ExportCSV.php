<?php

namespace Drupal\redirect_sage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\redirect\Entity\Redirect;
use Symfony\Component\HttpFoundation\Response;

class ExportCSV extends FormBase {

  public function getFormId() {
    return 'redirect_sage_csv_export';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['mark'] = [
      '#markup' => '<p>'
        . t('Export redirects into CSV file.')
        . '</p>'
    ];

    $form['from-filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('`From` filter'),
      '#description' => $this->t('Filter by source - works as CONTAINS condition. Leading/Trailing slashes will be trimmed.')
    ];

    $form['code-filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('`Code` filter'),
      '#description' => $this->t('Filter by http code. Keep empty if it\'s not needed.')
    ];

    $form['lang-filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('`Language code` filter'),
      '#description' => $this->t('Filter by lang code. You may use \'und\' for \'Not specified\'.')
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // apply filters
    $redirectsQuery = \Drupal::entityQuery('redirect');

    $fromFilter = trim($form_state->getValue('from-filter'), " \n\r\t\v\0/");
    if ($fromFilter) {
      $redirectsQuery->condition('redirect_source__path', $fromFilter, 'CONTAINS');
    }
    $codeFilter = $form_state->getValue('code-filter');
    if ($codeFilter) {
      $redirectsQuery->condition('status_code', $codeFilter);
    }
    $langFilter = $form_state->getValue('lang-filter');
    if ($langFilter) {
      $redirectsQuery->condition('language', $langFilter);
    }
    $redirectsQuery->sort('redirect_source__path');
    $ids = $redirectsQuery->execute();

    if (count($ids)) {
      // prepare export response
      $redirects = Redirect::loadMultiple($ids);
      $out = fopen('php://memory', 'r+');
      foreach($redirects as $redirect) {
        $src = $redirect->getSource();
        $dst = $redirect->getRedirect();
        $srcURL = Url::fromUserInput('/' . $src['path'], ['query' => $src['query']]);
        $dstURL = Url::fromUri($dst['uri'], $dst['options']);

        fputcsv($out, [
          $srcURL->toString(),
          $dstURL->toString(),
          $redirect->get('language')->value,
          $redirect->getStatusCode(),
        ]);
      }
      rewind($out);
      $response = new Response(stream_get_contents($out), 200, [
        'Content-Type' => 'text/csv',
        'Content-Transfer-Encoding' => 'binary',
        'Content-Disposition: attachment;filename=' . 'redirect-export.csv'
      ]);
      $form_state->setResponse($response);
    } else {
      \Drupal::messenger()->addStatus(t('No records to export!'));
    }
  }
}
