<?php

namespace Drupal\redirect_sage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ImportCSV extends FormBase {

  public function getFormId() {
    return 'redirect_sage_batch_csv_import';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['mark'] = [
      '#markup' => '<p>'
        . t('CSV format: one row - one redirect. Each row contains comma separated values - source, destination, language code, status code.')
        . '<br />'
        . t('Source and Destination are required fields. Language code by default - und or default language. Status code default value is 301.</p>')
        . '</p>'
    ];

    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Source type'),
      '#required' => TRUE,
      '#options' => [
        'text' => $this->t('text field'),
        'file' => $this->t('file'),
      ],
    ];

    $form['redirects'] = [
      '#type' => 'textarea',
      '#title' => 'Redirects',
      '#rows' => 15,
      '#description' => t('Provide redirects here as a text.'),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'text'],
        ],
      ],
    ];

    $form['redirectsFile'] = [
      '#type' => 'file',
      '#title' => 'Redirects',
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'file'],
        ],
      ],
    ];

    $form['submit_button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('type') == 'text') {
      $redirectsText = $form_state->getValue('redirects');
    } else {
      $all_files = \Drupal::request()->files;
      $file = $all_files->get('files')['redirectsFile'];
      $redirectsText = file_get_contents($file->getRealPath());
    }

    $rows = [];
    $rows = explode("\n", $redirectsText);

    $batch = array(
      'title' => t('Importing redirects...'),
      'operations' => [],
      'init_message'     => t('Heating Up'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message'    => t('An error occurred during processing'),
      'finished' => '\Drupal\redirect_sage\RedirectImport::finishedCallback',
    );
    foreach ($rows as $i => $row) {
      $batch['operations'][] = ['\Drupal\redirect_sage\RedirectImport::ImportLine', [$row, $i]];
    }

    \Drupal::logger('import sage')->notice('Import Sage batch started.');
    batch_set($batch);
  }
}
