<?php

namespace Drupal\redirect_sage;

use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl;
use Drupal\redirect\Entity\Redirect;

class RedirectImport {

  public static function skipRow(string $raw, array &$context): void {
    if (empty($context['results']['skipped'])) {
      $context['results']['skipped'] = 1;
    } else {
      $context['results']['skipped'] ++;
    }
  }

  public static function insertRow(string $raw, array &$context): void {
    if (empty($context['results']['inserted'])) {
      $context['results']['inserted'] = 1;
    } else {
      $context['results']['inserted'] ++;
    }
  }

  public static function updateRow(string $raw, array &$context): void {
    if (empty($context['results']['updated'])) {
      $context['results']['updated'] = 1;
    } else {
      $context['results']['updated'] ++;
    }
  }

  /**
   * Process one $raw line of redirect import
   *
   * @param string $raw: CSV line
   *
   * @param int $i: line number
   *
   * @param array &$context: batch context
   *
   * @return void
   */
  public static function ImportLine(string $raw, int $i, array &$context): void {
    $data = explode(',', $raw);
    [$source, $langCode] = self::parseLangCode($data[0]);
    [$source, $sourceQuery] = self::parseQuery($source);

    if (empty($data[1])) {
      self::skipRow($raw, $context);
      \Drupal::logger('import sage')->error('Destination is required field. Line #@i. @import', [
        '@i' => $i,
        '@import' => $raw
      ]);

    } else {
      [$destination, $destinationQuery] = self::parseQuery($data[1]);

      // langueage code
      if (!empty($data[2])) {
        $forcedLangCode = trim($data[2]);
        if ($forcedLangCode == 'und') {
          $codeFound = TRUE;
          $langCode = $forcedLangCode;
        } else {
          $codeFound = FALSE;
          foreach (\Drupal::languageManager()->getLanguages() as $language) {
            if ($forcedLangCode == $language->getId()) {
              $langCode = $forcedLangCode;
              $codeFound = TRUE;
              break;
            }
          }
        }
        if (!$codeFound) {
          \Drupal::logger('import sage')->warning('Required language is not installed. Line #@i. @import', [
            '@i' => $i,
            '@import' => $raw
          ]);
          self::skipRow($raw, $context);
          return;
        }
      }

      // Status code
      $statusCode = 301;
      if (!empty($data[3])) {
        $requiredStatusCode = (int) ($data[3]);
        if ($requiredStatusCode > 300 && $requiredStatusCode < 400) {
          $statusCode = $requiredStatusCode;
        } else {
          \Drupal::logger('import sage')->notice('Status code is out of bounds ~ [301, 400). Line #@i. @import', [
            '@i' => $i,
            '@import' => $raw
          ]);
        }
      }

      $hash = Redirect::generateHash($source, $sourceQuery, $langCode);
      $ids = \Drupal::entityQuery('redirect')
        ->condition('hash', $hash)
        ->execute();

      if (count($ids)) {
        // update
        $redirect = Redirect::load(reset($ids));
        $redirect->setRedirect($destination, $destinationQuery);
        $redirect->setStatusCode($statusCode);
        $redirect->save();
        self::updateRow($raw, $context);
      } else {
        // insert
        $redirect = Redirect::create([]);
        $redirect->setRedirect($destination, $destinationQuery);
        $redirect->setSource($source, $sourceQuery);
        $redirect->setLanguage($langCode);
        $redirect->setStatusCode($statusCode);
        $redirect->save();
        self::insertRow($raw, $context);
      }
    }

  }

  /**
   * Parse query params into separeted array
   * @return [ string uri, array params ]
   */
  public static function parseQuery(string $source): array {
    $args = [];
    [$source, $params] = explode('?', trim($source));
    parse_str($params, $args);
    return [trim($source, '/'), $args];
  }

  /**
   * Parse language code from given path
   * @return [ string path-without-langcode, string langcode ]
   */
  public static function parseLangCode(string $path): array {
    $path = trim($path, " \n\r\t\v\0/");

    if (\Drupal::languageManager()->isMultilingual()) {
      $langCode = null;

      $config = \Drupal::config('language.negotiation')->get('url');
      if ($config['source'] == LanguageNegotiationUrl::CONFIG_PATH_PREFIX) {
        $parts = explode('/', $path);
        $prefix = array_shift($parts);

        // Search prefix within added languages.
        foreach (\Drupal::languageManager()->getLanguages() as $language) {
          $enlistedLangCode = $language->getId();
          if ($language->isDefault() && is_null($langCode)) {
            $langCode = $enlistedLangCode;
            continue;
          }
          if (isset($config['prefixes'][$enlistedLangCode]) && $config['prefixes'][$enlistedLangCode] == $prefix) {
            // Rebuild $path with the language removed.
            $path = implode('/', $parts);
            $langCode = $enlistedLangCode;
            break;
          }
        }
      }

    } else {
      $langCode = 'und';
    }

    return [$path, $langCode];
  }

  /**
   * Batch callback function to report about final status.
   */
  public static function finishedCallback($success, $results, $operations, $elapsed) {
    if ($success) {
      $results = array_merge(['skipped' => 0, 'inserted' => 0, 'updated' => 0], $results);
      \Drupal::messenger()->addStatus(t('Processed @processed items. Exported: @correct (updated: @updated / inserted: @inserted), skipped: @skipped',
        [
          '@processed' => $results['inserted'] + $results['skipped'] + $results['updated'],
          '@correct' => $results['inserted'] + $results['updated'],
          '@updated' => $results['updated'],
          '@inserted' => $results['inserted'],
          '@skipped' => $results['skipped'],
        ]));
      if (!empty($results['skipped'])) {
        \Drupal::messenger()->addStatus(t('Details about skipped lines read in <a href="/admin/reports/dblog">status log</a>.'));
      }
    }
    else {
      \Drupal::messenger()->addError(t('Export process failed. Please review existing redirections or contact an administrator.'));
    }
  }

}